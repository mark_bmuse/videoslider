//
//  AppDelegate.h
//  Sessions Menu
//
//  Created by Mark Voskresenskiy on 03.09.15.
//  Copyright (c) 2015 Mark Voskresenskiy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

