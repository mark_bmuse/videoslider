//
//  ViewController.m
//  VideoLayerSlider
//
//  Created by Mark Voskresenskiy on 02.09.15.
//  Copyright (c) 2015 Mark Voskresenskiy. All rights reserved.
//

#import "ViewController.h"
#import "VideoView.h"

#define HIEGHT_SCALE_VALUE (self.view.bounds.size.width/4)
#define DIFF_SCALE_VALUE 0.5

@interface ViewController ()<VideoViewDelegate>
{
    VideoView * _videoView1;
    VideoView * _videoView2;
    VideoView * _videoView3;
    UIScrollView *_scrollView;
}

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.view setBackgroundColor:[UIColor blackColor]];
    
    _scrollView = [[UIScrollView alloc] initWithFrame:self.view.bounds];
    [self.view addSubview:_scrollView];
    
    _videoView1 = [[VideoView alloc] initWithFrame:self.view.bounds];
//    NSString * videoPath1 = [[NSBundle mainBundle] pathForResource:@"videos/Acc_endtag" ofType:@"mp4"];
//    [_videoView1 setVideoPath:videoPath1];
    [_videoView1 setImage:[UIImage imageNamed:@"A_card-1.png"]];
    _videoView1.maskToSwipeTouch = YES;
    _videoView1.delegate = self;
    [self.view addSubview:_videoView1];
    
    _videoView2 = [[VideoView alloc] initWithFrame:self.view.bounds];
    NSString * videoPath2 = [[NSBundle mainBundle] pathForResource:@"videos/content_loop_tall_a" ofType:@"mp4"];
    [_videoView2 setVideoPath:videoPath2];
    [_scrollView addSubview:_videoView2];
    
    CGRect frameVideoView3 = self.view.bounds;
    frameVideoView3.origin.y = CGRectGetMaxY(_videoView2.frame);
    _videoView3 = [[VideoView alloc] initWithFrame:frameVideoView3];
    NSString * videoPath3 = [[NSBundle mainBundle] pathForResource:@"videos/content_loop_tall_b" ofType:@"mp4"];
    [_videoView3 setVideoPath:videoPath3];
    [_scrollView addSubview:_videoView3];
    
    [_scrollView setContentSize:CGSizeMake(_videoView2.frame.size.width, 2*_videoView2.frame.size.height)];
    [self _scaleScrollView:1.0+DIFF_SCALE_VALUE];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [_videoView2 playVideo];
    [_videoView3 playVideo];
}

#pragma mark - VideoViewDelegate methods

- (void)_scaleScrollView:(CGFloat)scale
{
    _scrollView.transform = CGAffineTransformMakeScale(scale, scale);
}

- (void)videoView:(VideoView *)sender didChangeLayerFrame:(CGRect)layerFrame
{
    CGFloat diffScale = (-layerFrame.origin.y/HIEGHT_SCALE_VALUE)*DIFF_SCALE_VALUE;
    
    if(diffScale > 0.5)
    {
        diffScale = 0.5;
    }
    else if (diffScale < 0)
    {
        diffScale = 0.0;
    }
    
    [self _scaleScrollView:1.0+(DIFF_SCALE_VALUE - diffScale)];
}

@end
