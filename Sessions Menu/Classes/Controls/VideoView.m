//
//  VideoView.m
//  VideoLayerSlider
//
//  Created by Mark Voskresenskiy on 02.09.15.
//  Copyright (c) 2015 Mark Voskresenskiy. All rights reserved.
//

#import "VideoView.h"
#import <AVFoundation/AVFoundation.h>

#define GRADIENT_HEIGHT 100

@interface VideoView ()
{
    AVPlayer * _player;
    AVPlayerItem * _playerItem;
    AVPlayerLayer * _playerLayer;
    
    UIImageView * _imageView;
    
    BOOL _maskToSwipeTouch;
    CALayer * _currentLayer;
    CALayer * _maskLayer;
    CALayer * _alphaLayer;
    CAGradientLayer * _gradientLayer;
    
    UIPanGestureRecognizer * _panRecognizer;
}

@end

@implementation VideoView

- (void)dealloc
{
    if(self.delegate)
    {
        self.delegate = nil;
    }
    
    if(_player)
    {
        [_player pause];
        _player = nil;
    }
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (instancetype)init
{
    self = [super init];
    if(self)
    {
        [self _initializeVideoPlayer];
        self.maskToSwipeTouch = NO;
        
        _panRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(_panGestureRecognizer:)];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if(self)
    {
        [self _initializeVideoPlayer];
        self.maskToSwipeTouch = NO;
        
        _panRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(_panGestureRecognizer:)];
    }
    return self;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    if(_imageView)
    {
        _imageView.frame = self.bounds;
    }
    
    _currentLayer.frame = self.bounds;
    _maskLayer.frame = self.bounds;
    _alphaLayer.frame = self.bounds;
}

#pragma mark - publick methods

- (void)playVideo
{
    [_player play];
}

- (void)pauseVideo
{
    [_player pause];
}

- (void)setImage:(UIImage*)image
{
    if(!_imageView)
    {
        _imageView = [[UIImageView alloc] initWithFrame:self.bounds];
        [self.layer addSublayer:_imageView.layer];
    }
    
    if(_playerLayer)
    {
        [_playerLayer removeFromSuperlayer];
        _playerLayer = nil;
    }
    
    _imageView.image = image;
    _currentLayer = _imageView.layer;
}

- (void)setVideoBackgroundColor:(UIColor *)backgroundColor
{
    if(_playerLayer)
    {
        _playerLayer.backgroundColor = backgroundColor.CGColor;
    }
}

- (void)setVideoPath:(NSString*)videoPath
{
    NSURL * videoUrl = [NSURL fileURLWithPath:videoPath];
    if(videoUrl)
    {
        [self setVideoURL:videoUrl];
    }
}

- (void)setVideoURL:(NSURL*)videoUrl
{
    AVAsset *asset = ([videoUrl isFileURL]
                      ? [AVAsset assetWithURL:videoUrl]
                      : [AVURLAsset assetWithURL:videoUrl]);
    
    if(_playerItem)
    {
        [[NSNotificationCenter defaultCenter] removeObserver:self];
        _playerItem = nil;
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(_playerItemDidReachEnd:)
                                                 name:AVPlayerItemDidPlayToEndTimeNotification
                                               object:_playerItem];
    
    _playerItem = [[AVPlayerItem alloc] initWithAsset:asset];
    [_player replaceCurrentItemWithPlayerItem:_playerItem];
    
    if(!_playerLayer)
    {
        _playerLayer = [AVPlayerLayer playerLayerWithPlayer:_player];
        _playerLayer.frame = self.bounds;
        _playerLayer.backgroundColor = [UIColor blackColor].CGColor;
        [self.layer addSublayer:_playerLayer];
    }
    _currentLayer = _playerLayer;
    if(_imageView)
    {
        [_imageView.layer removeFromSuperlayer];
        _imageView = nil;
    }
}

- (BOOL)maskToSwipeTouch
{
    return _maskToSwipeTouch;
}

- (void)setMaskToSwipeTouch:(BOOL)maskToSwipeTouch
{
    if(_maskToSwipeTouch == maskToSwipeTouch)
        return;
    
    _maskToSwipeTouch = maskToSwipeTouch;
    if(_maskToSwipeTouch)
    {
        _maskLayer = [CALayer layer];
        _maskLayer.backgroundColor = [UIColor clearColor].CGColor;
        _maskLayer.frame = _currentLayer.bounds;
        _currentLayer.mask = _maskLayer;
        
        _alphaLayer = [CALayer layer];
        _alphaLayer.backgroundColor = [UIColor blackColor].CGColor;
        _alphaLayer.frame = _currentLayer.bounds;
        [_maskLayer addSublayer:_alphaLayer];
        
        _gradientLayer = [CAGradientLayer layer];
        _gradientLayer.colors = @[(id)[UIColor blackColor].CGColor, (id)[UIColor clearColor].CGColor];
        _gradientLayer.startPoint = CGPointMake(0, 0);
        _gradientLayer.endPoint = CGPointMake(0, 1);
        _gradientLayer.frame = CGRectMake(0, _alphaLayer.bounds.size.height, _alphaLayer.bounds.size.width, GRADIENT_HEIGHT);
        [_alphaLayer addSublayer:_gradientLayer];

        [self addGestureRecognizer:_panRecognizer];
    }
    else
    {
        _currentLayer.mask = nil;
        _maskLayer = nil;
        _alphaLayer = nil;
        [self removeGestureRecognizer:_panRecognizer];
    }
}

#pragma mark - private methods

- (void)_initializeVideoPlayer
{
    _player = [[AVPlayer alloc] init];
    _player.actionAtItemEnd = AVPlayerActionAtItemEndNone;
}

- (void)_playerItemDidReachEnd:(NSNotification *)notification
{
    AVPlayerItem *playerItem = [notification object];
    if(playerItem == _playerItem)
    {
        [playerItem seekToTime:kCMTimeZero];
    }
}

#pragma mark - Gesture Recognizer methods

- (void)_panGestureRecognizer:(UIPanGestureRecognizer*)recognizer
{
    switch (recognizer.state)
    {
        case UIGestureRecognizerStateBegan:
        {
            break;
        }
            
        case UIGestureRecognizerStateChanged:
        {
            CGPoint translation = [recognizer translationInView:self];
            CGPoint centerLayer = _alphaLayer.position;
            centerLayer.y = centerLayer.y + translation.y;
            _alphaLayer.position = centerLayer;
            
            if(_alphaLayer.frame.origin.y>0)
            {
                _alphaLayer.frame = self.bounds;
            }
            
            if(self.delegate && [self.delegate respondsToSelector:@selector(videoView:didChangeLayerFrame:)])
            {
                [self.delegate videoView:self didChangeLayerFrame:_alphaLayer.frame];
            }
            
            [recognizer setTranslation:CGPointZero inView:self];
            break;
        }
            
        case UIGestureRecognizerStateEnded:
        {
            CGRect frameLayer = _alphaLayer.frame;
            
            if(frameLayer.origin.y< -(frameLayer.size.height/2))
            {
                frameLayer.origin.y = -frameLayer.size.height;
            }
            else
            {
                frameLayer.origin.y = 0;
            }
            
            [UIView animateWithDuration:0.5 animations:^
            {
                _alphaLayer.frame = frameLayer;
            }
                             completion:^(BOOL finished)
            {
                if(_alphaLayer.frame.origin.y<0)
                {
                    [self setHidden:YES];
                }
                
                if(self.delegate && [self.delegate respondsToSelector:@selector(videoView:didChangeLayerFrame:)])
                {
                    [self.delegate videoView:self didChangeLayerFrame:_alphaLayer.frame];
                }
            }];
            
            break;
        }
            
        default:
            break;
    }
}

@end
