//
//  VideoView.h
//  VideoLayerSlider
//
//  Created by Mark Voskresenskiy on 02.09.15.
//  Copyright (c) 2015 Mark Voskresenskiy. All rights reserved.
//

#import <UIKit/UIKit.h>

@class VideoView;
@protocol VideoViewDelegate <NSObject>

@optional
- (void)videoView:(VideoView *)sender didChangeLayerFrame:(CGRect)layerFrame;

@end

@interface VideoView : UIView

@property (nonatomic,weak) id <VideoViewDelegate> delegate;
@property (nonatomic,assign) BOOL maskToSwipeTouch;

- (void)setImage:(UIImage*)image;
- (void)setVideoPath:(NSString*)videoPath;
- (void)setVideoURL:(NSURL*)videoUrl;

- (void)playVideo;
- (void)pauseVideo;
- (void)setVideoBackgroundColor:(UIColor *)backgroundColor;

@end
